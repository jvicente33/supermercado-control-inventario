
package proyecto_bd_fx;

import static java.awt.SystemColor.window;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;


public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TextField cedula;
    @FXML
    private PasswordField clave;
    @FXML
    private AnchorPane ventana_login, ventana_registro;

    Funciones f=new Funciones();
    Conexion c = new Conexion();
    String fecha;
    
    @FXML
    private void handleButtonAction(ActionEvent event) { 
    }

    
    @FXML
    private void imprimir() throws Exception{
 
        String userr=cedula.getText();
        String clavee=clave.getText();
        String clave_aux=f.getMD5(clavee);
        
        c.connect();
        int res=c.login(userr, clave_aux);
        c.close();
        
         if(res==0){
            JOptionPane.showMessageDialog(null, "Cedula no registrada en BD");
        }if(res==1){
            f.intercambio("Panel.fxml", ventana_login);
            
            c.connect();
            c.eliminarAuxiliar();
            c.close();
            
            c.connect();
            c.auxiliar(userr);
            c.close();
            
            c.connect();
            String usuario=c.leerAuxiliar();
            c.close();
            
            c.connect();
            c.registros(usuario,"Inicio Session",fecha);
            c.close();
           
        }if(res==2){
            JOptionPane.showMessageDialog(null, "Clave incorrecta");
        }

    }
    
    @FXML
    private void registro() throws IOException{
        Funciones f=new Funciones();
        f.intercambio("Registro.fxml", ventana_login);
        RegistroController rc=new RegistroController();
        //rc.insertarId();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        fecha=String.valueOf(dateFormat.format(date));
       
    }
    
    
    
}
