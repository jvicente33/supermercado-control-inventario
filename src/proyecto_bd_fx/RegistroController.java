package proyecto_bd_fx;

import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class RegistroController implements Initializable {

    @FXML
    private AnchorPane ventana_registro;
    @FXML
    private TextField id;
    @FXML
    private TextField primer_nombre;
    @FXML
    private TextField segundo_nombre;
    @FXML
    private TextField primer_apellido;
    @FXML
    private TextField segundo_apellido;
    @FXML
    private TextField cedula;
    @FXML
    public TextField rif;
    @FXML
    private DatePicker nacimiento;
    @FXML
    private CheckBox check_v;
    @FXML
    private CheckBox check_e;
    @FXML
    private ComboBox<?> sexo;
    @FXML
    private TextField correo;
    @FXML
    private TextField celular;
    @FXML
    private TextField telefono;
    
    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        insertarId();
       
    }    
    
    @FXML
    private void siguiente() throws IOException{
        f.intercambio("Registro2.fxml", ventana_registro);
    }
    
    @FXML
    private void cancelar() throws IOException{
        f.intercambio("login.fxml", ventana_registro);
        
    }
    
    @FXML
    private void insertarId(){
        c.connect();
        int id_aux=c.sumarId();
        c.close();
        
        String id_string=Integer.toString(id_aux);
        id.setText(id_string);
    }
    
    @FXML
    private void registrarEmpleado1() throws SQLException, IOException{
        
        String pn, sn, pa, sa, sexoo, correoo, ps, nacionalidad, cedulaa, riff, celularr, telefonoo;
        int idd;
        nacionalidad=null;
        
        pn=primer_nombre.getText();
        sn=segundo_nombre.getText();
        pa=primer_apellido.getText();
        sa=primer_apellido.getText();
        cedulaa=cedula.getText();
        riff=rif.getText();
        
        
        boolean v=check_v.isSelected();
        boolean e=check_v.isSelected();
        if((v==true)&&(e==false)){
            nacionalidad="v";
           }
        if((v==false)&&(e==true)){
            nacionalidad="e";
           }
        else{
            nacionalidad="v";
        }
        
        sexoo=(String) sexo.getValue();
        
        LocalDate date = nacimiento.getValue();
        correoo=correo.getText();
        celularr=celular.getText();
        telefonoo=telefono.getText();
        
        Funciones f=new Funciones();
        ps=f.preguntaSecreta();
        
        
        Conexion c=new Conexion();
        c.connect();
        idd=c.sumarId();
        String ida=Integer.toString(idd);
        c.registrar1(idd,pn,sn,pa,sa,Integer.parseInt(cedulaa),Integer.parseInt(riff),sexoo,String.valueOf(date),correoo,celularr,telefonoo,idd,idd,idd,idd,idd,nacionalidad);
        c.close();
        
        siguiente();
    }
    
    
}
