package proyecto_bd_fx;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javax.swing.JOptionPane;


public class Registro4Controller implements Initializable {
    
    @FXML
    private AnchorPane ventana_registro;
    @FXML
    private ImageView foto;
    @FXML
    private CheckBox check_ad;
    @FXML
    private CheckBox check_al;
    @FXML
    private CheckBox check_mo;
    @FXML
    private CheckBox check_fi;
    @FXML
    private CheckBox check_te;

    
    File ruta;
    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }    
    
    @FXML
    public void buscar(){
        //Buscar Foto
        //buscar_foto.setOnAction(event -> {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Buscar Foto");

        // Agregar filtros para facilitar la busqueda
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        // Obtener la imagen seleccionada
        File imgFile = fileChooser.showOpenDialog(null);
        ruta=imgFile;
        //f.copiarArchivos(ruta);
        // Mostar la imagen
        if (imgFile != null) {
            Image image = new Image("file:" + imgFile.getAbsolutePath());
            foto.setImage(image);
        }
    //});
    }
    
    @FXML
    private void cancelar() throws IOException{
        f.intercambio("login.fxml", ventana_registro);
    }
    
    @FXML
    private void finalizarR() throws IOException{
        JOptionPane.showMessageDialog(null, "Registro finalizado con exito");
        f.intercambio("login.fxml", ventana_registro);
    }
    
    @FXML
    private void empleadosFoto() throws IOException{
        String ti = null, ta = null, ruta_aux, nombre_foto;
        int idd;
 
        c.connect();
        idd=c.sumarId();
        idd=idd-1;
        c.close();
        
        nombre_foto="foto"+Integer.toString(idd)+".jpg";
        ruta_aux=f.copiarArchivos(ruta, nombre_foto);
        
        boolean ad=check_ad.isSelected();
        boolean al=check_al.isSelected();
        boolean mo=check_mo.isSelected();
        
        if((ad==true)&&(al==false)&&(mo==false)){
            ti="Administrativo";
        }
        else if((ad==false)&&(al==true)&&(mo==false)){
            ti="Almacenista";
        }
        else if((ad==false)&&(al==false)&&(mo==true)){
            ti="Montacargas";
        }else{
            ti="No seleccionado";
        }
        
        boolean fi=check_fi.isSelected();
        boolean te=check_te.isSelected();
        
        if ((fi==true)&&(te==false)){
            ta="Fijo";
        }
        else if ((fi==false)&&(te==true)){
            ta="Temporal";
        }else{
            ta="No seleccionado";
        }
        
        c.connect();
        c.registrarFoto(idd, ruta_aux);
        c.close();
        
        c.connect();
        c.registrar4(idd, ti, ta);
        c.close();
        
        c.connect();
        c.registrarLogin(idd);
        c.close();
        
        finalizarR();
        
        
        
    }
    
}
