
package proyecto_bd_fx;

import javafx.beans.property.SimpleStringProperty;


public class Almacen2 {
    
    private final SimpleStringProperty nombre;
    private final SimpleStringProperty proveedor; 
    private final SimpleStringProperty cantidad; 
    private final SimpleStringProperty precio; 
    private final SimpleStringProperty fecha;
    private final SimpleStringProperty id;
    
    Almacen2(String name, String prove, String cant, String pre, String fechaa, String idd){
        this.nombre=new SimpleStringProperty(name);
        this.proveedor=new SimpleStringProperty(prove);
        this.cantidad=new SimpleStringProperty(cant);
        this.precio=new SimpleStringProperty(pre);
        this.fecha=new SimpleStringProperty(fechaa);
        this.id=new SimpleStringProperty(idd);
    }
    
    public String getNombre() {
            return nombre.get();
        }
    public void setNombre(String name) {
            nombre.set(name);
        }
    public String getProveedor() {
            return proveedor.get();
        }
    public void setProveedor(String prove) {
            proveedor.set(prove);
    }
    public String getCantidad() {
            return cantidad.get();
        }
    public void setCantidad(String cant) {
            cantidad.set(cant);
        }
    public String getPrecio() {
            return precio.get();
        }
    public void setPrecio(String pre) {
            precio.set(pre);
        }
    public String getFecha() {
            return fecha.get();
        }
    public void setFecha(String fechaa) {
            fecha.set(fechaa);
        }
    public String getId() {
            return id.get();
        }
    public void setId(String idd) {
            id.set(idd);
        }
    
    
}
