
package proyecto_bd_fx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;


public class ClientesController implements Initializable {

    @FXML
    private AnchorPane ventana_clientes;
    @FXML
    private TextField id;
    @FXML
    private TextField nombre;
    @FXML
    private TextField cedula_rif;

    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    int id_int;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        c.connect();
        id_int=c.idAlmacen("clientes");
        c.close();
        id.setText(Integer.toString(id_int));
    }    
    
    @FXML
    private void siguiente() throws IOException{
        f.intercambio("Direccion_clientes.fxml",ventana_clientes);
    }
    
    @FXML
    private void cancelar() throws IOException{
        f.intercambio("Panel.fxml",ventana_clientes);
    }
    
    @FXML
    private void ingresar() throws IOException{
        
        
        String nombree, cedula;
        
        nombree=nombre.getText();
        cedula=cedula_rif.getText();
        
        c.connect();
        c.ingresarClientes(id_int, nombree, cedula);
        c.close();
        
        siguiente(); 
    }
    
}
